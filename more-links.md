---
description: More links about me.
cover: .gitbook/assets/Kurapika Kurta ⛓️❤️💛.jpeg
coverY: -86
---

# 📝 More Links



{% tabs %}
{% tab title="Evrim Ağacı" %}
[https://evrimagaci.org/serce](https://evrimagaci.org/serce)
{% endtab %}

{% tab title="LinkTree" %}
[https://linktr.ee/sercee](https://linktr.ee/sercee)
{% endtab %}

{% tab title="PPF.ONE" %}
[https://ppf.one/sercee](https://ppf.one/sercee)
{% endtab %}

{% tab title="solo.to" %}
[https://solo.to/deadnorth](https://solo.to/deadnorth)
{% endtab %}
{% endtabs %}

