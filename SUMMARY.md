# Table of contents

* [👋 Oi!](README.md)
* [👨💻 Software](software.md)
* [🎊 Activities](activities.md)
* [📝 Links](links.md)
* [📝 More Links](more-links.md)
* [🍀 Discord Servers](discord-servers.md)
* [📧 Contact](contact.md)
